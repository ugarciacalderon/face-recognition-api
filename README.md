# Construit imagen Docker

- sudo docker build -t biometricos-api .  

# Iniciar contenedor

- sudo docker run -p 8000:8000 -t biometricos-api


# Iniciar solo api, sin el contenedor docker

- uvicorn main:app --host 0.0.0.0 --port 80


## Resolver error que no muestra la rama principal al clonar el repositorio
- git switch main


## Actual error arrojado al estar conectado a la vpn institucional, sino esta conectado no arroja error (Linux)
ERROR: failed to solve: continuumio/miniconda3: failed to copy: httpReadSeeker: failed open: failed to do request: Get "https://production.cloudflare.docker.com/registry-v2/docker/registry/v2/blobs/sha256/a1/a101d1f8cd1cbfc6b0e91e4f1ce03e48a6bdca7987168647044a497e57411c56/data?verify=1696965896-%2BkYE%2BRSFDoC4QqmlJStsOlyk4Eg%3D": dial tcp: lookup production.cloudflare.docker.com: i/o timeout


## Posible solucion al error, estando conectado a la vpn anyconnect cisco
https://github.com/rancher-sandbox/rancher-desktop/issues/2477