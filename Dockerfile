FROM continuumio/miniconda3

RUN conda install -c conda-forge dlib -y

RUN yes | pip install cmake
RUN yes | pip install face_recognition
RUN yes | pip install fastapi
RUN yes | pip install "uvicorn[standard]"
#RUN yes | python -m pip install cx_Oracle --upgrade

LABEL key="ulises.garciac@ine.mx"

COPY ./biometrics-api /fastapi
#COPY configuracion.sh /fastapi
WORKDIR /fastapi/

ENTRYPOINT uvicorn --host 0.0.0.0 main:app --reload